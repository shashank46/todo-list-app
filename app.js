var express = require('express');
var app = express();

var todoController = require('./controllers/todoController.js');
// set up template engine

app.set('view engine','ejs');

// static files

app.use(express.static('./public'));

// fire controllers
todoController(app);

// listen to port

app.listen(3000);
console.log('you are listening to port 3000');

